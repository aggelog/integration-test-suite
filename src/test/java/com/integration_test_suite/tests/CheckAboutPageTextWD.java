package com.integration_test_suite.tests;


import com.integration_test_suite.DriverFactory;
import com.integration_test_suite.page_factory_objects.IndexPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class CheckAboutPageTextWD extends DriverFactory{

    @Test
    public void goToTheAboutPage() throws Exception {
        WebDriver driver = getDriver();
        driver.get("http://localhost:8090/index.html");

        indexPage = new IndexPage();

        assertThat(indexPage.mainTextIsDisplayed(),is(equalTo(true)));
        assertThat(indexPage.mainPageButtonIsDisplayed(), is(equalTo(true)));

        aboutPage = indexPage.footer.goToTheAboutUsPage();

        assertThat(aboutPage.aboutUsTextIsDisplayed(), is(equalTo(true)));
    }
}
